from django.contrib import admin
from  pizzashopapp.models import PizzaShop, Pizza

admin.site.register(PizzaShop)
admin.site.register(Pizza)

